package core

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"

	"gopkg.in/yaml.v2"
)

type Configuration struct {
	AppName    string `yaml:"app_name"`
	AppVersion string `yaml:"app_version"`
	AppPort    string `yaml:"app_port"`
	Author     string `yaml:"author"`

	LimitItems float64 `yaml:"limit_items"`

	DBHost     string `yaml:"db_host"`
	DBPort     string `yaml:"db_port"`
	DBName     string `yaml:"db_name"`
	DBUser     string `yaml:"db_user"`
	DBPassword string `yaml:"db_password"`

	ElasticHost string `yaml:"elastic_host"`
	ElasticPort string `yaml:"elastic_port"`

	Secret               string `yaml:"secret"`
	UploadCarePublicKey  string `yaml:"uploadcare_public_key"`
	UploadCarePrivateKey string `yaml:"uploadcare_private_key"`

	Dev        bool `yaml:"dev"`
	SQLLogMode bool `yaml:"sql_log_mode"`
	Seed       bool `yaml:"seed"`
}

var Config *Configuration

func LoadConfig(fileName string) {
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(data, &Config)
	if err != nil {
		panic(err)
	}

	Config.DBHost = getEnv("DBHost", Config.DBHost)
	Config.DBName = getEnv("DBName", Config.DBName)
	Config.DBPort = getEnv("DBPort", Config.DBPort)
	Config.DBUser = getEnv("DBUser", Config.DBUser)
	Config.DBPassword = getEnv("DBPassword", Config.DBPassword)
	Config.ElasticHost = getEnv("ElasticHost", Config.ElasticHost)
	Config.ElasticPort = getEnv("ElasticPort", Config.ElasticPort)
	Config.Secret = getEnv("Secret", Config.Secret)

	dev, _ := strconv.ParseBool(getEnv("DEV", strconv.FormatBool(Config.Dev)))
	Config.Dev = dev

	seed, _ := strconv.ParseBool(getEnv("Seed", strconv.FormatBool(Config.Seed)))
	Config.Seed = seed

	sqlLogMode, _ := strconv.ParseBool(getEnv("SQLLogMode", strconv.FormatBool(Config.SQLLogMode)))
	Config.SQLLogMode = sqlLogMode

	limitString := fmt.Sprintf("%f", Config.LimitItems)
	limitItems, _ := strconv.ParseFloat(getEnv("LimitItems", limitString), 64)
	Config.LimitItems = limitItems

	printInfo()

	if dev {
		gin.SetMode("debug")
	} else {
		gin.SetMode("release")
	}
}

func getEnv(key string, defaultValue string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return defaultValue
}

func printInfo() {
	mode := "PRODUCTION"
	if Config.Dev {
		mode = "DEVELOPMENT"
	}

	fmt.Println("---------------------------------------")
	fmt.Println(fmt.Sprintf(`- %v`, Config.AppName))
	fmt.Println(fmt.Sprintf(`- Version: %v`, Config.AppVersion))
	fmt.Println(fmt.Sprintf(`- Environment: %v`, mode))
	fmt.Println(fmt.Sprintf(`- Author: %v`, Config.Author))
	fmt.Println("---------------------------------------")
}
