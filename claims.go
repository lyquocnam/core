package core

import (
	jwt "github.com/dgrijalva/jwt-go"
	uuid "github.com/satori/go.uuid"
)

type Claims struct {
	jwt.StandardClaims
	ID    *uuid.UUID `json:"id"`
	Roles string     `json:"roles"`
}
