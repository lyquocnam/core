package core

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// DB Gorm Database
var DB *gorm.DB

//
// Connect to database
//
func Connect() {
	db, err := gorm.Open("postgres",
		fmt.Sprintf(`host=%v port=%v dbname=%v user=%v password=%v sslmode=disable`,
			Config.DBHost,
			Config.DBPort,
			Config.DBName,
			Config.DBUser,
			Config.DBPassword))

	if err != nil {
		panic(err)
	}

	db.LogMode(Config.Dev)

	DB = db
}
