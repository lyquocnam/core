package core

import (
	"fmt"
	"os"
	"regexp"
	"time"

	"github.com/badoux/checkmail"
	"golang.org/x/crypto/bcrypt"
	mapper "gopkg.in/jeevatkm/go-model.v1"
)

//
// Kiểm tra email
//
func IsEmail(email string) bool {
	//isEmail, _ := regexp.MatchString(`^[A-Za-z0-9_\.]{5,32}@([a-zA-Z0-9]{2,12})(\.[a-zA-Z]{2,12})+$`, email)
	err := checkmail.ValidateFormat(email)
	return err == nil
}

//
// Kiểm tra số điện thoại
//
func IsPhoneNumber(phone string) bool {
	isPhone, _ := regexp.MatchString(`^\d{3,15}$`, phone)
	return isPhone
}

//
// Hàm mã hoá mật khẩu
//
func HashPassword(password string) string {
	hashed, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		fmt.Println(err)
	}
	return string(hashed)
}

//
// Hàm so sánh mật khẩu
//
func ComparePassword(hashedPassword string, plainPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(plainPassword))
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

//
// Lấy thời gian cuối ngày
//
func BeginOfDay(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, GetLocation())
}

//
// Lấy thời gian cuối ngày
//
func EndOfDay(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 23, 59, 59, 999999999, GetLocation())
}

//
// Hàm lấy location mặc định (bỏ qua location) cho kiểu time.
//
func GetLocation() *time.Location {
	//loc, _ := time.LoadLocation("Asia/Ho_Chi_Minh")
	//return loc
	return time.FixedZone("", 0)
}

//
// Copy interface 1 to interface 2
//
func Copy(input interface{}, output interface{}) []error {
	return mapper.Copy(output, input)
}

//
// CreateTmpFolderIfNotExist Create Tmp folder for uploads
func CreateTmpFolderIfNotExist() {
	if _, err := os.Stat("tmp"); os.IsNotExist(err) {
		err = os.Mkdir("tmp", os.ModePerm)
		if err != nil {
			fmt.Println(err)
		}
	}
}
